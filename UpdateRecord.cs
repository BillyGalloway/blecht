﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkillCheckE
{
    internal class UpdateRecord
    {
        internal int AccountID { get; set; }
        internal Decimal TransactionAmount { get; set; }
        internal String InputRecord { get; set; }
        internal bool ValidRecord { get; set; }
    }
}
