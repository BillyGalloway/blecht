﻿/* Project Name:    SkillCheckEUnitTester
 * Developer:       Carol C. Torkko, MCSD - Bellevue College
 * Date:            August, 2013
 * Description:     Includes tests for testing skill check D.  Applies the features built
 *                              into the nunit.framework library.
*/

using System;
using SkillCheckE;
using NUnit.Framework;
using System.Reflection;

namespace SkillCheckEUnitTester
{
    [TestFixture]
    public class UnitTests
    {
        [Test]
        public void AccountClassShouldBeAbstract()
        {
            Type accountType = GetClassType("Account");

            Assert.IsTrue(accountType != null, "Account class should exist.");

            Assert.IsTrue(accountType.IsAbstract, "Account class should be abstract.");
        }

        [Test]
        public void GetClientInformationMethodInAccountShouldBeAbstract()
        {
            Type accountType = GetClassType("Account");
            MethodInfo getClientInformationMethod = accountType.GetMethod("GetClientInformation",
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            Assert.IsTrue(getClientInformationMethod.IsAbstract, "GetClientInformation method in Account class should be abstract.");
        }

        [Test]
        public void ArrayInBankClassShouldNotExist()
        {
            Type bankType = GetClassType("Bank");
            FieldInfo accountsArrayMember = bankType.GetField("accounts",
                BindingFlags.Instance | BindingFlags.NonPublic);

            Assert.IsTrue(accountsArrayMember == null, "Array named accounts still exists.  The declaration should be removed.");
        }

        [Test]
        public void ListCollectionInBankAccountsClassShouldExist()
        {
            Type bankType = GetClassType("BankAccounts");
            FieldInfo accountsArrayMember = bankType.GetField("_accounts",
                BindingFlags.Instance | BindingFlags.NonPublic);

            Assert.IsTrue(accountsArrayMember != null, "Generic List<> collection of Account type should be declared with the name accounts.");

            Assert.IsTrue(accountsArrayMember.IsPrivate, "_accounts generic List<> collection should not be outwardly accessible.");
        }

        [Test]
        public void AccountsPropertyInBankAccountsClassShouldHaveInaccessibleGetAccessor()
        {
            bool expectedTrue = VerifyAccountsPropertyHasGetAccessor("Accounts");

            Assert.IsTrue(expectedTrue, "Get accessor of Accounts property should not be outwardly accessible.");
        }

        [Test]
        public void AccountsPropertyInBankAccountsClassShouldHaveInaccessibleSetAccessor()
        {
            bool expectedTrue = VerifyAccountsPropertyHasSetAccessor("Accounts");

            Assert.IsTrue(expectedTrue, "Set accessor of Accounts property should not be outwardly accessible.");
        }

        [Test]
        public void UpdatesToCDBalanceShouldBeValid()
        {
            Account account = new CD(84127, "Melissa Thompson", 20280.00m, DateTime.Parse("5/30/2012"), 250.00m);

            try
            {
                account.UpdateBalance(350.00m);
                Assert.True(false,
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=350.00: UpdateBalance in Checking should raise an exception because update amount greater than the allowed maximum.");
            }
            catch (Exception ex)
            {
                TransactionOutOfRangeException transException = ex as TransactionOutOfRangeException;
                Assert.IsTrue(transException != null,
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=350.00: Exception raised in UpdateBalance in Checking should be the TransactionOutOfRangeException exception.");
                Assert.IsTrue(transException.CustomerAccount.Equals(account),
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=350.00: Exception should include a reference to the customer's checking account.");
                Assert.IsTrue(transException.TransactionAmount == 350.00m,
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=350.00: Exception should include the transaction amount.");
                Assert.IsTrue(transException.Message == "Transaction amount must be greater than 0 and less than or equal to $250. Transaction is rejected.",
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=350.00: Exception message should be as expected.");
            }

            try
            {
                account.UpdateBalance(-200.00m);
                Assert.True(false,
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=-200.00: UpdateBalance in Checking should raise an exception because update amount is less than zero.");
            }
            catch (Exception ex)
            {
                TransactionOutOfRangeException transException = ex as TransactionOutOfRangeException;
                Assert.IsTrue(transException != null,
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=-200.00: Exception raised in UpdateBalance in Checking should be the TransactionOutOfRangeException exception.");
                Assert.IsTrue(transException.CustomerAccount.Equals(account),
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=-200.00: Exception should include a reference to the customer's checking account.");
                Assert.IsTrue(transException.TransactionAmount == -200.00m,
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=-200.00: Exception should include the transaction amount.");
                Assert.IsTrue(transException.Message == "Transaction amount must be greater than 0 and less than or equal to $250. Transaction is rejected.",
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=-200.00: Exception message should be as expected.");
            }

            try
            {
                account.UpdateBalance(0.00m);
                Assert.True(false,
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=0.00: UpdateBalance in Checking should raise an exception because update amount is zero.");
            }
            catch (Exception ex)
            {
                TransactionOutOfRangeException transException = ex as TransactionOutOfRangeException;
                Assert.IsTrue(transException != null,
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=0.00: Exception raised in UpdateBalance in Checking was not the TransactionOutOfRangeException.");
                Assert.IsTrue(transException.CustomerAccount.Equals(account),
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=0.00: Exception should include a reference to the customer's checking account.");
                Assert.IsTrue(transException.TransactionAmount == 0.00m,
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=0.00: Exception should include the transaction amount.");
                Assert.IsTrue(transException.Message == "Transaction amount must be greater than 0 and less than or equal to $250. Transaction is rejected.",
                    "Melissa Thompson: Beg. Balance=20280.00: Trans=0.00: Exception message should be as expected.");
            }

            try
            {
                account.UpdateBalance(240.00m);
                Assert.IsTrue(account.Balance == 20520.00m, "Melissa Thompson: Beg. Balance=20280.00: Trans=240.00: UpdateBalance in Checking should update the balance accurately.");
            }
            catch
            {
                Assert.True(false, "Melissa Thompson: Beg. Balance=20280.00: Trans=240.00: UpdateBalance in Checking should not raise an exception.");
            }

            String accountInfoString = account.GetClientInformation();
            string expectedReturnString =
                "\nAccount Type:\tCD\nClient Account ID:\t84127\nClient Name:\tMelissa Thompson\nBalance: \t$20,520.00\nCD End Date: \t5/30/2012\n";
            Assert.AreEqual(expectedReturnString, accountInfoString,
                "Return value from GetClientInformation in CD should be the expected value.");

            Assert.AreEqual("Melissa Thompson", account.ClientName,
                "ClientName property should be setting/getting the correct value.");

            Assert.AreEqual(20520.00m, account.Balance,
                "Balance property should be setting/getting the correct value.");

            Assert.AreEqual(84127, account.AccountID,
                "AccountID property should be setting/getting the correct value.");
        }

        [Test]
        public void UpdatesToCheckingBalanceShouldBeValid()
        {
            Account account = new Checking(28651, "Tom Adams", 734.89m, 120.00m);

            try
            {
                account.UpdateBalance(-834.13m);
                Assert.True(false,
                    "Tom Adams: Balance=734.89: Trans=-834.23: UpdateBalance in Checking should raise an exception for balance less than zero.");
            }
            catch (Exception ex)
            {
                TransactionOutOfRangeException transException = ex as TransactionOutOfRangeException;
                Assert.IsTrue(transException != null,
                    "Tom Adams: Balance=734.89: Trans=-834.23: Exception raised in UpdateBalance in Checking should be the TransactionOutOfRangeException exception.");
                Assert.IsTrue(transException.CustomerAccount.Equals(account),
                    "Tom Adams: Balance=734.89: Trans=-834.23: Exception should include a reference to the customer's checking account.");
                Assert.IsTrue(transException.TransactionAmount == -834.13m,
                    "Tom Adams: Balance=734.89: Trans=-834.23: Exception should include the transaction amount.");
                Assert.IsTrue(transException.Message == "Transaction amount causes an overflow condition. Transaction is rejected.",
                    "Tom Adams: Balance=734.89: Trans=-834.23: Exception message should be the expected value.");
            }

            try
            {
                account.UpdateBalance(-630.00m);
                Assert.True(false,
                    "Tom Adams: Beg. Balance=734.89: Trans=-630.00: UpdateBalance in Checking should raise an exception for balance below the minimum.");
            }
            catch (Exception ex)
            {
                TransactionOutOfRangeException transException = ex as TransactionOutOfRangeException;
                Assert.IsTrue(transException != null,
                    "Tom Adams: Beg. Balance=734.89: Trans=-630.00: Exception raised in UpdateBalance in Checking should be the TransactionOutOfRangeException exception.");
                Assert.IsTrue(transException.CustomerAccount.Equals(account),
                    "Tom Adams: Beg. Balance=734.89: Trans=-630.00: Exception should include a reference to the customer's checking account.");
                Assert.IsTrue(transException.TransactionAmount == -630.00m,
                    "Tom Adams: Beg. Balance=734.89: Trans=-630.00: Exception should include the transaction amount.");
                Assert.IsTrue(transException.Message == "Transaction amount would bring the balance below the minimum accepted. Transaction is rejected.",
                    "Tom Adams: Beg. Balance=734.89: Trans=-630.00: Exception message should be the expected value.");
            }

            try
            {
                account.UpdateBalance(1800.00m);
                Assert.IsTrue(account.Balance == 2534.89m,
                    "Tom Adams: Beg. Balance=734.89: Trans=1800.00: UpdateBalance in Checking should update the balance accurately.");
            }
            catch
            {
                Assert.True(false,
                    "Tom Adams: Beg. Balance=734.89: Trans=1800.00: UpdateBalance in Checking should not raise an exception.");
            }

            String accountInfoString = account.GetClientInformation();
            string expectedReturnString =
                "\nAccount Type:\tChecking\nClient Account ID:\t28651\nClient Name:\tTom Adams\nBalance: \t$2,534.89\nMinimum Balance Allowed: \t$120.00\n";
            Assert.AreEqual(expectedReturnString, accountInfoString,
                "Return value from GetClientInformation in Checking should be the expected value.");


            Assert.AreEqual("Tom Adams", account.ClientName,
                "ClientName property should be setting/getting the correct value.");

            Assert.AreEqual(2534.89m, account.Balance,
                "Balance property should be setting/getting the correct value.");

            Assert.AreEqual(28651, account.AccountID,
                "AccountID property should be setting/getting the correct value.");
        }

        [Test]
        public void AccountShouldHaveClientNameProperty()
        {
            VerifyAccountClassHasProperty("ClientName");
        }

        [Test]
        public void AccountShouldHaveAccountIDProperty()
        {
            VerifyAccountClassHasProperty("AccountID");
        }

        [Test]
        public void ExceptionShouldHaveConstructorWithNoArguments()
        {
            TransactionOutOfRangeException transException = new TransactionOutOfRangeException();
            Assert.IsTrue(transException.Message == "Transaction is invalid as the amount is outside of the allowed range.",
                "Message from TransactionOutOfRangeException exception should be the default message.");
        }

        [Test]
        public void ExceptionShouldHaveConstructorWithOneArgument()
        {
            TransactionOutOfRangeException transException = new TransactionOutOfRangeException("Testing constructor with single parameter.");
            Assert.IsTrue(transException.Message == "Testing constructor with single parameter.",
                "Message from TransactionOutOfRangeException exception should be the same as the input to the contructor.");
        }

        [Test]
        public void ExceptionShouldHaveConstructorWithTwoArguments()
        {
            TransactionOutOfRangeException transException =
                new TransactionOutOfRangeException("Testing constructor with two parameters.",
                    new ArgumentException("Testing inner exception value of TransactionOutOfRangeException exception."));
            Assert.IsTrue(transException.Message == "Testing constructor with two parameters.",
                "Message from TransactionOutOfRangeException exception should be the same as the input to the constructor.");
            Assert.AreNotEqual(transException.InnerException, null,
                "Inner Exception property of TransactionOutOfRangeException exception should not be null.");
            ArgumentException ae = transException.InnerException as ArgumentException;
            Assert.AreNotEqual(ae, null,
                "Inner Exception property of TransactionOutOfRangeException exception should be the same type as input to the constructor.");
        }

        [Test]
        public void VerifyIndexerInBankAccountsClass()
        {
            Account account = new CD(84127, "Melissa Thompson", 20280.00m, DateTime.Parse("5/30/2012"), 250.00m);

            BankAccounts ba = new BankAccounts(1);

            // Compile errors will exist until the indexer is implemented in the BankAccounts class.
            ba[84127] = account;

            account = ba[84127];
            Assert.IsTrue(account != null, "Indexer in BankAccounts class should allow ability to set/get an account from the collection.");

        }

        private Type GetClassType(string name)
        {
            var assembly = Assembly.ReflectionOnlyLoadFrom("SkillCheckE.exe");
            var types = assembly.GetTypes();
            foreach (var type in types)
            {
                if (type.Name == name)
                    return type;
            }
            return null;
        }

        private void VerifyAccountClassHasProperty(string propertyName)
        {
            bool foundSetBool = false;
            string setAccessorName = "set_" + propertyName;

            // Verify ClientName property exists in Account class.
            Type accountType = GetClassType("Account");
            PropertyInfo propertyInfo = accountType.GetProperty(propertyName);

            // Verify Set accessor exists in ClientName property and is set to private.
            MethodInfo[] propertyAccessors = propertyInfo.GetAccessors(true);

            foreach (MethodInfo mi in propertyAccessors)
            {
                if (mi.Name == setAccessorName)
                {
                    // Verify Set accessor is private.
                    Assert.IsTrue(mi.IsPrivate == true, "Set accessor of " + propertyName +
                        " property should not be outwardly accessible.");
                    foundSetBool = true;
                }
            }

            // Verify Set accessor exists.
            Assert.IsTrue(foundSetBool, "Set accessor in " + propertyName +
                " property should exist.");
        }

        private bool VerifyAccountsPropertyHasGetAccessor(string propertyName)
        {
            string getAccessorName = "get_" + propertyName;

            // Verify property exists in BankAccounts class.
            Type bankAccountsType = GetClassType("BankAccounts");
            PropertyInfo propertyInfo = bankAccountsType.GetProperty(propertyName,
                BindingFlags.NonPublic | BindingFlags.Instance);

            // Verify both accessors exist in property and are private.
            MethodInfo[] propertyAccessors = propertyInfo.GetAccessors(true);

            // Locate get accessor and make sure it is private.
            foreach (MethodInfo mi in propertyAccessors)
            {
                if (mi.Name == getAccessorName)
                {
                    // Verify Get accessor is private.
                    if (mi.IsPrivate)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool VerifyAccountsPropertyHasSetAccessor(string propertyName)
        {
            string setAccessorName = "set_" + propertyName;

            // Verify property exists in BankAccounts class.
            Type bankAccountsType = GetClassType("BankAccounts");
            PropertyInfo propertyInfo = bankAccountsType.GetProperty(propertyName,
                BindingFlags.NonPublic | BindingFlags.Instance);

            // Verify both accessors exist in property and are private.
            MethodInfo[] propertyAccessors = propertyInfo.GetAccessors(true);

            // Locate set accessor and make sure it is private.
            foreach (MethodInfo mi in propertyAccessors)
            {
                if (mi.Name == setAccessorName)
                {
                    // Verify Set accessor is private.
                    if (mi.IsPrivate)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
