﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkillCheckE
{
    internal enum AccountType
    {
        Checking,
        CD,
        Unknown
    }
    
    internal class AccountRecord
    {
        internal int AccountID { get; set; }
        internal string ClientName { get; set; }
        internal Decimal BalanceDecimal { get; set; }
        internal Decimal MinBalanceDecimal { get; set; }
        internal DateTime EndDate { get; set; }
        internal AccountType AccountType { get; set; }
        internal bool ValidRecord { get; set; }        
    }
}
