﻿/* Project Name:    SkillCheckF
 * Class Name:      InputFile
 * Developer:       Carol C. Torkko, MCSD - Bellevue College
 * Date:            August, 2013
 * Description:     Opens, reads and closes the outside file.  Returns data to either the InputAccountDataFile class or
 *                              the InputUpdateAccountfile class.
 */

using System;
using System.IO;

namespace SkillCheckE
{
    abstract class InputFile
    {
        private static StreamReader _dataFileStreamReader = null;

        private static StreamReader OpenFile(string fileNameString)
        {
            try
            //Open file
            {
                DataFile = new StreamReader(fileNameString);
                return DataFile;
            }
            catch
            {
                Console.WriteLine(
                    "File {0} can not be found. Please locate and store it into the bin/debug folder of this project.",
                    fileNameString);
                return null;
            }
        }

        // Only method in this class that is accessible by derived classes.
        protected static String[] ReadRecord(string fileNameString, out string inputRecordString)
        {
            inputRecordString = null;
            char[] delimiters = { '\t' };

            // First time call to ReadRecord the DataFile will not be open.
            if (DataFile == null)
            {
                OpenFile(fileNameString);

                //  null means file could not be open.
                if (DataFile == null)
                {
                    return null;
                }
            }

            // Read data, create instance with data
            if (DataFile.EndOfStream)
            {
                CloseFile();
                // null means no more records.
                return null;
            }

            inputRecordString = DataFile.ReadLine();
            return inputRecordString.Split(delimiters);
        }

        internal static void CloseFile()
        {
            if (DataFile != null)
            {
                // Close file
                DataFile.Close();
                DataFile = null;
            }
        }

        private static StreamReader DataFile
        {
            get { return _dataFileStreamReader; }
            set { _dataFileStreamReader = value; }
        }
    }
}
