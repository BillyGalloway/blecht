﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkillCheckE
{
    class Checking : Account
    {
        private decimal _minBalanceDecimal;

        internal Checking(int accountID, string clientNameString, decimal balanceDecimal
            , decimal minBalanceDecimal) :
            base(accountID, clientNameString, balanceDecimal)
        {
            MinBalance = minBalanceDecimal;
        }

        internal decimal MinBalance
        {
            get { return _minBalanceDecimal; }
            private set { _minBalanceDecimal = value; }
        }

        internal override string GetClientInformation()
        {
            string clientInfoString = null;

            clientInfoString = "\nAccount Type:\tChecking";
            clientInfoString += "\nClient Account ID:\t" + AccountID.ToString();
            clientInfoString += "\nClient Name:\t" + ClientName;
            clientInfoString += "\nBalance: \t" + Balance.ToString("C");
            clientInfoString += "\nMinimum Balance Allowed: \t" + MinBalance.ToString("C") + "\n";

            return clientInfoString;
        }

        internal override void UpdateBalance(decimal transactionAmountDecimal)
        {
            string messageString = null;
            decimal balanceDecimal = Balance + transactionAmountDecimal;
            if (balanceDecimal < MinBalance)
            {
                if (balanceDecimal < 0)
                {
                    messageString = "Transaction amount causes an overflow condition. Transaction is rejected.";
                }
                else
                {
                    messageString = "Transaction amount would bring the balance below the minimum accepted. Transaction is rejected.";
                }

                TransactionOutOfRangeException ex = new TransactionOutOfRangeException(
                    messageString);
                ex.CustomerAccount = this;
                ex.TransactionAmount = transactionAmountDecimal;
                throw ex;
            }
            else
            {
                Balance = balanceDecimal;
            }
        }
    }
}
