﻿/* Project Name:    SkillCheckF
 * Class Name:      InputUpdateAccountFile
 * Developer:       Carol C. Torkko, MCSD - Bellevue College
 * Date:            August, 2013
 * Description:   Reads the data file to get balance update information for the main program.  
 *                              Calls InputFile class to get an update record and examines the contents of the data.
 *                              Each record is added to the collection.  After all records have been read and processed,
 *                              the collection is returned to the main program.
 */

using System;
using System.Collections.Generic;

namespace SkillCheckE
{
    class InputUpdateAccountFile : InputFile
    {
        private const string _DATA_FILE_NAME_String = "SkillCheckEAccountUpdate.txt";

        // Only method accessible by main program in this class.
        internal static List<UpdateRecord> GetUpdateRecords()
        {
            List<UpdateRecord> updateRecords = new List<UpdateRecord>();

            String inputRecordString = null;
            String[] fieldsStringArray  = null;
            UpdateRecord updateRecord = null;
            Decimal transactionDecimal = 0.0m;
            int accountID = 0;

            do
            {
                fieldsStringArray = ReadRecord(_DATA_FILE_NAME_String, out inputRecordString);

                if (fieldsStringArray == null)
                {
                    continue;
                }

                updateRecord = new UpdateRecord();
                updateRecord.ValidRecord = true;
                transactionDecimal = 0.0m;
                accountID = 0;

                if (fieldsStringArray.Length == 2)
                {
                    // Make sure first value in input record is a numeric value for the account id.
                    if (!int.TryParse(fieldsStringArray[0], out accountID))
                    {
                        Console.WriteLine(
                            "Error: The value of the account ID in the following record is not numeric: {0}",
                            inputRecordString);
                        
                        updateRecord.ValidRecord = false;
                    }

                    // Make sure second value in input record is a numeric value for the transaction amount.
                    if (!decimal.TryParse(fieldsStringArray[1], out transactionDecimal))
                    {
                        Console.WriteLine("Error: The value of the transaction in the following record is not numeric: {0}",
                            inputRecordString);
                       
                        updateRecord.ValidRecord = false;
                    }

                }
                else
                {
                    Console.WriteLine("Error: The following update record does not have two values: {0}",
                        inputRecordString);
                    
                    updateRecord.ValidRecord = false;
                }

                // Load values into update record before adding it into the collection to be returned back to Main.
                updateRecord.AccountID = accountID;
                updateRecord.TransactionAmount = transactionDecimal;
                updateRecord.InputRecord = inputRecordString;
                updateRecords.Add(updateRecord);
            } while (fieldsStringArray != null);

            return updateRecords;
        }
    }
}
