﻿/* Project Name:    SkillCheckE
 * Developer:       Carol C. Torkko, MCSD - Bellevue College
 * Date:            February, 2011
 * Description:     Create and use a service class that contains a generic List collection, indexer and
 *                      other supporting methods.
 * Revision Name: Include Unit testing.
 * Developer:       Carol C. Torkko, MCSD - Bellevue College
 * Date:            August, 2013
 * Description:     Reorganize structure of program to incorporate unit testing, and expand the skill
 *                              check.
 */

using System;
using System.Collections.Generic;

namespace SkillCheckE
{

    //TODO: 04. NOTE: The accounts are now stored in a generic List collection of Account type in the
    //                      BankAccounts class.  In previous skill checks, the account information was stored in
    //                      an Account type array that was declared within the Bank class.  Nothing to do here.
    //                      (1 min.)
    
    // Bank class
    class Bank
    {
        private const decimal _MAX_CD_TRANSACTION_AMOUNT_Decimal = 250.00m;
        private const int _MAX_NUMBER_OF_ACCOUNTS_Integer = 2;

        static void Main()
        {
            Account clientAccount = null;
            List<AccountRecord> accountRecords = null;
            List<UpdateRecord> updateRecords = null;

            try
            {

                // TODO: 05. Declare and create an instance of the BankAccounts class passing in the
                //          constant _MAX_NUMBER_OF_ACCOUNTS_Integer to the constructor.
                //          REMEMBER THE REFERENCE NAME YOU ASSIGN HERE as you will need it in
                //          the following TODOs (2 min.).

                BankAccounts BA = new BankAccounts(_MAX_NUMBER_OF_ACCOUNTS_Integer);
                
                //------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                //  Get account records from input file and store each acount record into the accounts collection.
                //------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                accountRecords = InputAccountDataFile.GetAccountRecords();

                // Loop through each account record.
                foreach (AccountRecord accountRecord in accountRecords)
                {
                    // Bypass record if not valid.
                    if (!accountRecord.ValidRecord)
                    {
                        continue;
                    }

                    clientAccount = null;

                    // Create an instance of the Account class passing in the client information.
                    switch (accountRecord.AccountType)
                    {
                        case AccountType.Checking:

                            //  Create an instance of the Checking type.
                            clientAccount = new Checking(
                                accountRecord.AccountID, accountRecord.ClientName, accountRecord.BalanceDecimal,
                                accountRecord.MinBalanceDecimal);
                            break;

                        case AccountType.CD:

                            //  Create an instance of the CD type.
                            clientAccount = new CD(
                                accountRecord.AccountID, accountRecord.ClientName, accountRecord.BalanceDecimal, accountRecord.EndDate,
                                _MAX_CD_TRANSACTION_AMOUNT_Decimal);
                            break;

                        default:
                            break;
                    }

                    if (clientAccount == null)
                    {
                        continue;
                    }
                    else
                    {
                        //TODO: 06. Use the indexer in the BankAccounts class to add the client 
                        //          account (reference stored in the clientAccount variable) into the
                        //          collection.  Use the AccountID property within the clientAccount 
                        //          reference as the index (3 min.).
                        BA[clientAccount.AccountID] = clientAccount;
                    }

                    // Remove reference to Account.
                    clientAccount = null;
                }   // Loop back up to process next account record.

                //------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                // Get the update records from the data file containing transaction amounts to update the
                //      balance in each of the 2 account types.
                //------------------------------------------------------------------------------------------------------------------------------------------------------------------------

                updateRecords = InputUpdateAccountFile.GetUpdateRecords();

                // Loop through each update record.
                foreach (UpdateRecord updateRecord in updateRecords)
                {
                    // Bypass record is not valid.
                    if (!updateRecord.ValidRecord)
                    {
                        continue;
                    }

                    // Get the account in the accounts collection.
                    //TODO: 07. Use the indexer to retrieve a reference to the client account that is stored
                    //          in the collection in the BankAccounts class. The index is the account ID, which
                    //          is stored in the updateRecord object.  Store the return value from the 
                    //          indexer into the clientAccount variable (3 min.).
                    clientAccount = BA[updateRecord.AccountID];

                    // Display error message if account is not found in the collection and bypass the record.
                    if (clientAccount == null)
                    {
                        Console.WriteLine(
                            "Error: There is no account for the account id in this record: {0}\n",
                            updateRecord.InputRecord);
                        continue;
                    }

                    // If any business rule is violated in the UpdateBalance method,  the
                    //      TransactionOutOfRangeException exception will be thrown.
                    try
                    {
                        // Using polymorphism, update the balance in the account from the 
                        //      transaction amount.
                        clientAccount.UpdateBalance(updateRecord.TransactionAmount);
                    }
                    catch (TransactionOutOfRangeException ex)
                    {
                        Console.WriteLine("ERROR: {0}\n\tTransaction amount is: {1}.\nAccount: {2}\n",
                            ex.Message, ex.TransactionAmount, ex.CustomerAccount.GetClientInformation());

                        continue;   // Bypass update record.
                    }

                    // Replace the account instance in the collection with the updated account instance.
                    //TODO: 08. Overlay the existing account in the collection in the BankAccounts class by
                    //          using the indexer.  Use the account id in the clientAccount object
                    //          as the index and clientAccount variable as the value to be passed in (3 min.).
                    

                }  // Loop back up to read and process the next update record.

                //Print the customer account information.
                //TODO: 09. Call the PrintAccounts method in the BankAccounts service class
                //                      to print out the client information (1 min.).

                BA.PrintAccounts();
                //Make sure all messages on the console are read before program ends.
                Console.Write("Press any key to end the program.");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: The following exception has occurred:\n{0}\nApplication is now ending.",
                    ex.Message);
            }
        }
    }
}

