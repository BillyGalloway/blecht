﻿/* Project Name:    SkillCheckF
 * Class Name:      InputAccountDataFile
 * Developer:       Carol C. Torkko, MCSD - Bellevue College
 * Date:            August, 2013
 * Description:   Reads the data file to get account information for the main program.  
 *                              Calls InputFile class to get data for next record and examines the contents of the data.
 *                              Each data record is added to the collection. After all data records have been read and
 *                              processed, the collection is returned to the main program.
 */

using System;
using System.Collections.Generic;

namespace SkillCheckE
{
    class InputAccountDataFile : InputFile
    {
        private const string _DATA_FILE_NAME_String = "SkillCheckEAccountData.txt";

        // Only method accessible by main program in this class.
        internal static List<AccountRecord> GetAccountRecords()
        {
            List<AccountRecord> accountRecords = new List<AccountRecord>();

            String inputRecordString = null;
            String[] fieldsStringArray = null;
            AccountRecord accountRecord = null;
            decimal balanceDecimal = 0.0m;
            decimal minBalanceDecimal = 0.0m;
            DateTime endDate = DateTime.Parse("1/1/0001");
            int accountID = 0;
            string clientNameString = null;
            AccountType accountType = AccountType.Unknown;

            do
            {
                fieldsStringArray = InputFile.ReadRecord(_DATA_FILE_NAME_String, out inputRecordString);
                if (fieldsStringArray == null)
                {
                    continue;
                }

                accountRecord = new AccountRecord();
                accountRecord.ValidRecord = true;
                balanceDecimal = 0.0m;
                minBalanceDecimal = 0.0m;
                endDate = DateTime.Parse("1/1/0001");
                accountID = 0;
                clientNameString = null;
                accountType = AccountType.Unknown;

                if (fieldsStringArray.Length == 5)
                {
                    // Make sure first value in input record is a numeric value for the account id.
                    if (!int.TryParse(fieldsStringArray[0], out accountID))
                    {
                        Console.WriteLine(
                            "Error: The value of the account ID in the following record is not numeric: {0}",
                            inputRecordString);
                        
                        accountRecord.ValidRecord = false;
                    }

                    // Make sure fourth value in input record is a numeric value for the balance.
                    if (!decimal.TryParse(fieldsStringArray[3], out balanceDecimal))
                    {
                        Console.WriteLine("Error: The value of the balance in the following record is not numeric: {0}",
                            inputRecordString);
                        
                        accountRecord.ValidRecord = false;
                    }

                    // Supply a default value for the client name if the third value in input record is empty.
                    clientNameString = fieldsStringArray[2];
                    if (clientNameString.Length == 0)
                    {
                        clientNameString = "Unknown";
                    }

                    // Determine account type from the second field in the array.
                    switch (fieldsStringArray[1].ToUpper())
                    {
                        case "CHECKING":
                            // Make sure fifth value in input record is a numeric value for the minimum balance.
                            if (!decimal.TryParse(fieldsStringArray[4], out minBalanceDecimal))
                            {
                                Console.WriteLine("Error: The value of the minimum balance in the following record is not numeric: {0}",
                                    inputRecordString);
                                
                                accountRecord.ValidRecord = false;
                            }
                            accountType = AccountType.Checking;
                            break;

                        case "CD":
                            // Make sure fifth value in input record is a date for the ending date of the CD.
                            if (!DateTime.TryParse(fieldsStringArray[4], out endDate))
                            {
                                Console.WriteLine("Error: The value of the CD ending date in the following record is not a date: {0}",
                                    inputRecordString);
                                
                                accountRecord.ValidRecord = false;
                            }
                            accountType = AccountType.CD;
                            break;

                        default:
                            Console.WriteLine("Error: The following record does not have an account type of either Checking or CD: {0}",
                                inputRecordString);
                            
                            accountRecord.ValidRecord = false;
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Error: The following record does not have five values: {0}",
                        inputRecordString);
                    
                    accountRecord.ValidRecord = false;
                }

                // Load data into account record and store the record into the collection.
                accountRecord.AccountID = accountID;
                accountRecord.AccountType = accountType;
                accountRecord.BalanceDecimal = balanceDecimal;
                accountRecord.ClientName = clientNameString;
                accountRecord.EndDate = endDate;
                accountRecord.MinBalanceDecimal = minBalanceDecimal;
                accountRecords.Add(accountRecord);
            } while (fieldsStringArray != null);

            // Return the collection of records to the main program.
            return accountRecords;
        }
    }   // End of class
}   // End of namespace
