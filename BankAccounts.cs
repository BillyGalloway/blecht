﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SkillCheckE
{
    // BankAccounts class
    internal class BankAccounts
    {
        //TODO: 01. Declare a private generic List<> collection to contain only Account types
        //          and name it _accounts.  Also code the property for this field called Accounts
        //          that is to be available ONLY to this class and have the ability to set and 
        //          retrieve the collection as a whole. (5 min.).
        private List<Account> _accounts;
        
        private List<Account> Accounts
        {
            get { return _accounts; }
            set { _accounts = value; }
        }

        


        internal BankAccounts(int numberOfAccountsInteger)
        {
            // TODO: 02. Create an instance of the collection declared above specifying its initial 
            //      capacity based on the input parameter numberOfAccountsInteger (1 min.).
            Accounts = new List<Account>(numberOfAccountsInteger);
        }

        //TODO: 03. Create an indexer that retrieves and adds accounts (Account type) into the collection
        //          specified above (15 min.). The index passed in is the account ID, which is an int data type (use the
        //          variable name, accountIdInteger, for the name in the index). Use the blocks of code
        //          below for the get and set accessors:

        //          a. Get accessor code block (uncomment the code after inserting it into the accessor):

                    //Account account = null;

                    //foreach (Account acct in Accounts)
                    //{
                    //    if (acct.AccountID == accountIdInteger)
                    //    {
                    //        account = acct;
                    //        break;
                    //    }
                    //}

                    //return account;


        //          b. Set accessor code block (uncomment the code after inserting it into the accessor):

                    //// This accessor functions as two different operations. The
                    //// first is a replace if the account already exists. The 
                    //// second is an add to the end of the collection if the 
                    //// account does not exist.
                    //int indexInteger = Accounts.IndexOf(value);

                    //if (indexInteger == -1)
                    //{
                    //    Accounts.Add(value);
                    //}
                    //else
                    //{
                    //    Accounts[indexInteger] = value;
                    //}
        internal Account this[int accountIdInteger]
        {
            get
            {
                Account account = null;

                foreach (Account acct in Accounts)
                {
                    if (acct.AccountID == accountIdInteger)
                    {
                        account = acct;
                        break;
                    }
                }

                return account;
            }
            set
            {
                // This accessor functions as two different operations. The
                // first is a replace if the account already exists. The 
                // second is an add to the end of the collection if the 
                // account does not exist.
                int indexInteger = Accounts.IndexOf(value);

                if (indexInteger == -1)
                {
                    Accounts.Add(value);
                }
                else
                {
                    Accounts[indexInteger] = value;
                }
            }
        }


        internal void PrintAccounts()
        {
            //-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
            // Print client information.
            //------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            foreach (Account account in Accounts)
            {
                Console.WriteLine(account.GetClientInformation());
            }
        }
    }
}
